library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Processor_MIPS is
	port(
		clk : in std_logic;
		rst : in std_logic;

		bridge_A : out std_logic_vector(31 downto 0)

	);
end entity;

architecture MIPS_arc of Processor_MIPS is

	component ALU
	port(
			data_a : in std_logic_vector(31 downto 0);
			data_b : in std_logic_vector(31 downto 0);
			
			op_addr 		: in std_logic_vector(3 downto 0);
			op_compair 	: in std_logic_vector(2 downto 0);
			data_b_type : in std_logic;
			enable_op	: in std_logic;
			
			branch_enable	: out std_logic;
			data_out : out std_logic_vector(31 downto 0)
		);
	end component;

	component Reg_bank
		port(
			clk : in std_logic;
			rst : in std_logic;
			
			addr_rd1: in std_logic_vector(4 downto 0);
			addr_rd2: in std_logic_vector(4 downto 0);
			
			addr_wr : in std_logic_vector(4 downto 0);
			data_wr 		: in std_logic_vector(31 downto 0);
			wr_en			: in std_logic;

			data_wr_Link 	: in std_logic_vector(31 downto 0);
			wr_en_Link		: in std_logic;
			
			data_rd1 : out std_logic_vector(31 downto 0);
			data_rd2 : out std_logic_vector(31 downto 0)
		);
	end component;
	
	component Mem_data
		PORT
		(
			address		: IN STD_LOGIC_VECTOR (10 DOWNTO 0);
			clock		: IN STD_LOGIC  := '1';
			data		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			wren		: IN STD_LOGIC ;
			q		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
		);	
	end component;

	component Mem_program
		PORT
		(
			address		: IN STD_LOGIC_VECTOR (10 DOWNTO 0);
			clock		: IN STD_LOGIC  := '1';
			q		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
		);
	end component;
	
	component Control_instructions
		port (
			Instruct : in std_logic_vector(5 downto 0);
			Link_poss : in std_logic;
			
			wr_en_RAM : out std_logic;
			
			wr_en_Bk		: out std_logic;
			Dst_wr_Bk	: out std_logic;
			Type_imediate: out std_logic_vector(3 downto 0);
			Data_wr_chsr: out std_logic;
			
			IsLink		: out std_logic;
			
			JUMPn			: out std_logic;
			BEQn 			: out std_logic;
			BNEn    		: out std_logic;
			BLEZn   		: out std_logic;
			BGTZn			: out std_logic;
			ULA_COM_2n	: out std_logic;
			IsCOM1		: out std_logic
			
		);
	end component;
	
	component ALU_controller 
		port(		
			clk : in std_logic;
			-- immediate data type
			Type_imediate : in std_logic_vector(3 downto 0);
			IsImediate : in std_logic;
			
			-- reg_reg op
			IsCOM_1 			: in std_logic;
			Type_COM_1_op	: in std_logic_vector(5 downto 0);
			
			-- with branches
			ULA_COM_2n 	: in std_logic;
			BEQn 			: in std_logic;
			BNEn    		: in std_logic;
			BLEZn   		: in std_logic;
			BGTZn			: in std_logic;
			COM_2_bit	: in std_logic;
			
			-- outs for ALU
			ALU_data_b_type : out std_logic;
			ALU_address	 	 : out std_logic_vector(3 downto 0);
			Select_out_compair : out std_logic_vector(2 downto 0);
			ALU_Test			: out std_logic
		);
	end component;	
	
	signal data_a_ALU, data_b_ALU, data_out_ALU, data_out_ALU_reg, data_out_ALU_reg2, data_wr_Link : std_logic_vector(31 downto 0);
	signal branch_enable, wr_en_Link : std_logic; 
	signal op_ALU : std_logic_vector(3 downto 0);
	
	signal addr_Prog  : std_logic_vector(10 downto 0);
	signal out_Prog	: std_logic_vector(31 downto 0);
	
	
	signal wr_en_RAM, Dst_wr_Bk, Data_wr_chsr, Dst_wr_Bk_reg  : std_logic;
	signal JUMPn, IsLink, BEQn, BNEn, BLEZn, BGTZn, ULA_COM_2n, IsCOM_1 : std_logic;

	signal Type_imediate : std_logic_vector(3 downto 0);
	
	signal addr_Reg_1_Bk, addr_Reg_2_Bk, addr_wr_Reg_Bk, addr_wr_Bk_reg, addr_wr_Bk_reg2, addr_wr_Bk_reg3 : std_logic_vector(4 downto 0);
	signal addr_1_Bk_reg, addr_1_Bk_reg2, addr_1_Bk_reg3, addr_2_Bk_reg, addr_2_Bk_reg2, addr_2_Bk_reg3 : std_logic_vector(4 downto 0);
	signal data_Reg_1_Bk, data_Reg_2_Bk, data_wr_Reg_Bk : std_logic_vector(31 downto 0);
	signal wr_en_Bk, wr_en_reg, wr_en_reg2, wr_en_reg3, Link_poss : std_logic;
	
	signal ALU_data_b_type : std_logic;
	signal ALU_address	 	 : std_logic_vector(3 downto 0);
	signal Select_out_compair : std_logic_vector(2 downto 0);
	signal ALU_Test			: std_logic;
	
	signal out_Prog_extend : std_logic_vector(31 downto 0);
	
	signal hazard_ALU_0_1, hazard_ALU_0_2, hazard_ALU_1_1, hazard_ALU_1_2: std_logic;

begin
	
	process(clk, rst)
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				addr_Prog <= (others => '0'); 
			else
				addr_Prog <= std_logic_vector(unsigned(addr_Prog) + 1); 
			end if;
		end if;
	end process;
------------------------ Pipe 1 ------------------------------
	---------- Memoria de programa ----------
	------ possui latencia de 1 ciclo -------
	Mem_program_inst: Mem_program
		port map(
			address	=> addr_Prog,
			clock		=> clk,
			q			=> out_Prog
		);
	-----------------------------------------
	
	------- Controlador de instrucoes ---------
	--------- Componente Assincrono -----------
	Controller: Control_instructions
		port map (
			Instruct 		=> out_Prog(31 downto 26),
			Link_poss 		=> Link_poss,
			
			wr_en_RAM		=> wr_en_RAM,
			
			wr_en_Bk			=> wr_en_Bk,
			Dst_wr_Bk		=> Dst_wr_Bk,
			Type_imediate	=> Type_imediate,
			Data_wr_chsr	=> Data_wr_chsr,
			
			IsLink			=> IsLink,
		
			JUMPn				=> JUMPn,
			BEQn 				=> BEQn,
			BNEn    			=> BNEn,
			BLEZn   			=> BLEZn,
			BGTZn				=> BGTZn,
			ULA_COM_2n		=> ULA_COM_2n,
			IsCOM1			=> IsCOM_1
		);
	
--------------------------------------------------------------
	
--------------------------- Pipe 2 ---------------------------

	-------- Banco de Registradores ---------	
	------ possui latecia de 1 ciclo --------
	addr_Reg_1_Bk <= out_Prog(25 downto 21);
	addr_Reg_2_Bk <= out_Prog(20 downto 16);
	process(clk)
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				addr_wr_Bk_reg <= (others => '0');
				addr_wr_Bk_reg2 <= (others => '0');
				-- para o HAZARD
				addr_wr_Bk_reg3 <= (others => '0');
				addr_1_Bk_reg  <= (others => '0');
				addr_1_Bk_reg2 <= (others => '0');
				addr_1_Bk_reg3 <= (others => '0');
				addr_2_Bk_reg  <= (others => '0');
				addr_2_Bk_reg2 <= (others => '0');
				addr_2_Bk_reg3 <= (others => '0');
				---------------
				wr_en_reg <= '0';
				wr_en_reg2 <= '0';
				wr_en_reg3 <= '0';
				Dst_wr_Bk_reg <= '0';
				out_Prog_extend <= (others => '0');
			else
				-- para o HAZARD
				addr_wr_Bk_reg <= addr_wr_Reg_Bk;
				addr_wr_Bk_reg2 <= addr_wr_Bk_reg;
				addr_wr_Bk_reg3 <= addr_wr_Bk_reg2;
				addr_1_Bk_reg  <= addr_Reg_1_Bk;
				addr_1_Bk_reg2 <= addr_1_Bk_reg;
				addr_1_Bk_reg3 <= addr_1_Bk_reg2;
				addr_2_Bk_reg  <= addr_Reg_2_Bk;
				addr_2_Bk_reg2 <= addr_2_Bk_reg;
				addr_2_Bk_reg3 <= addr_2_Bk_reg2;
				----------------
				if out_Prog = (31 downto 0 => '0') then
					wr_en_reg <= '0';
				else
					wr_en_reg <= wr_en_Bk;
				end if;
				wr_en_reg2 <= wr_en_reg;
				wr_en_reg3 <= wr_en_reg2;
				Dst_wr_Bk_reg <= Dst_wr_Bk;
				out_Prog_extend <= (15 downto 0 => out_Prog(15)) & out_Prog(15 downto 0);
			end if;
		end if;
	end process;
	addr_wr_Reg_Bk <= out_Prog(20 downto 16) when Dst_wr_Bk = '1' 
						else out_Prog(15 downto 11);
	
	data_wr_Reg_Bk <= data_out_ALU_reg;
	
	Reg_bank_inst: Reg_bank
		port map(
			clk 		=> clk,
			rst 		=> rst,
			
			addr_rd1	=> addr_Reg_1_Bk,
			addr_rd2	=> addr_Reg_2_Bk,
			
			addr_wr 	=> addr_wr_Bk_reg2,
			data_wr 	=> data_wr_Reg_Bk,
			wr_en		=> wr_en_reg2,

			data_wr_Link	=> data_wr_Link,
			wr_en_Link		=> wr_en_Link,
			
			data_rd1 => data_Reg_1_Bk,
			data_rd2 => data_Reg_2_Bk
		);
	-----------------------------------------
	wr_en_Link <= '0';
	
	------------------ Controlador da ALU ----------------
	------------   Sincrona com latencia de 1 ------------
	ALU_controller_inst:ALU_controller 
		port map(		
			clk => clk,
			
			-- immediate data type
			Type_imediate =>	Type_imediate,
			IsImediate 	  =>	Dst_wr_Bk,
			
			-- reg_reg op
			IsCOM_1 		  => IsCOM_1,
			Type_COM_1_op => out_Prog(5 downto 0),
			
			-- with branches
			ULA_COM_2n 	  => ULA_COM_2n,
			BEQn 			  => BEQn,
			BNEn    		  => BNEn,
			BLEZn   		  => BLEZn,
			BGTZn			  => BGTZn,
			COM_2_bit	  => out_Prog(16),
			
			-- outs for ALU
			ALU_data_b_type 		=> ALU_data_b_type,
			ALU_address	 	 		=> ALU_address,
			Select_out_compair 	=> Select_out_compair,
			ALU_Test				 	=> ALU_Test
		);
	
	
	-------------- Assincrona ---------------
	data_a_ALU <= data_out_ALU_reg when hazard_ALU_0_1 = '1' else  -- Hazard nivel 1
					  data_out_ALU_reg2 when hazard_ALU_1_1 = '1' else -- Hazard nivel 2
					  data_Reg_1_Bk;
	data_b_ALU <= data_out_ALU_reg when hazard_ALU_0_2 = '1' else 	-- Hazard nivel 1
					  data_out_ALU_reg2 when hazard_ALU_1_2 = '1' else -- Hazard nivel 2
					  data_Reg_2_Bk when Dst_wr_Bk_reg = '0' else 
					  out_Prog_extend;
	op_ALU <= ALU_address;
	ALU_inst: ALU
		port map	(
			data_a 		=> data_a_ALU,
			data_b 		=> data_b_ALU,
			
			op_addr 		=> op_ALU,
			op_compair 	=> Select_out_compair,
			data_b_type => ALU_data_b_type,
			enable_op	=> ALU_Test,
			
			branch_enable	=> branch_enable,
			data_out 		=> data_out_ALU
		);
	-----------------------------------------
	
--------------------------------------------------------------

------------------------------- Pipe 3 -----------------------
	process(clk)
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				data_out_ALU_reg <= (others => '0');
				data_out_ALU_reg2 <= (others => '0');
			else 
				data_out_ALU_reg <= data_out_ALU;
				data_out_ALU_reg2 <= data_out_ALU_reg;
			end if;
		end if;
	end process;
	bridge_A <= data_out_ALU_reg;
-------------------------------------------------------------




----------------- HAZARD ----------------------

--------- para operacoes soma .. do tipo I e R
--------- o erro permanece dois ciclos apos a instrucao
--------- entre R1 e R2 esta a ALU
	hazard_ALU_0_1 <= '1' when addr_wr_Bk_reg2 = addr_1_Bk_reg and wr_en_reg2 = '1'
							else '0';
	hazard_ALU_0_2 <= '1' when addr_wr_Bk_reg2 = addr_2_Bk_reg and wr_en_reg2 = '1'
							else '0';
	hazard_ALU_1_1 <= '1' when addr_wr_Bk_reg3 = addr_1_Bk_reg and wr_en_reg3 = '1'
							else '0';
	hazard_ALU_1_2 <= '1' when addr_wr_Bk_reg3 = addr_2_Bk_reg and wr_en_reg3 = '1'
							else '0';
-----------------------------------------------
					
end MIPS_arc;