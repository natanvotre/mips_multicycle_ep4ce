library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity ALU_controller is 
	port(
		clk : in std_logic;
		-- immediate data type
		Type_imediate : in std_logic_vector(3 downto 0);
		IsImediate : in std_logic;
		
		-- reg_reg op
		IsCOM_1 			: in std_logic;
		Type_COM_1_op	: in std_logic_vector(5 downto 0);
		
		-- with branches
		ULA_COM_2n 	: in std_logic;
		BEQn 			: in std_logic;
		BNEn    		: in std_logic;
		BLEZn   		: in std_logic;
		BGTZn			: in std_logic;
		COM_2_bit	: in std_logic;
		
		-- outs for ALU
		ALU_data_b_type : out std_logic;
		ALU_address	 	 : out std_logic_vector(3 downto 0);
		Select_out_compair : out std_logic_vector(2 downto 0);
		ALU_Test			: out std_logic
	);
end entity;

architecture ALU_controller_arc of ALU_controller is
	signal COM_zero: std_logic;
	signal ALU_address_i: std_logic_vector(3 downto 0);
begin

	process (clk)
	begin
		if rising_edge(clk) then
			ALU_address <= ALU_address_i;
			ALU_Test <= Type_COM_1_op(4) or Type_COM_1_op(3);
		end if;
	end process;
	
	ALU_address_i <= Type_imediate when IsImediate = '1'
				else Type_COM_1_op(5) & Type_COM_1_op(2 downto 0);
	
	------- Operacoes que fazem comparação com zero
	COM_zero <= '1' when ALU_address_i(3 downto 1) = "101" else '0';
	process (clk)
	begin
		if rising_edge(clk) then
			ALU_data_b_type <= BGTZn or BLEZn or ULA_COM_2n or COM_zero;
		end if;
	end process;
	---------------------
	
	------------------- seleciona o tipo de comparacao para o branch ------------------
	process (clk)
	begin
		if rising_edge(clk) then
			if (BEQn = '1') then					-- X == Y	
		
				Select_out_compair <= "000";
			
			elsif (BNEn = '1') then				-- X != Y
			
				Select_out_compair <= "001";
			
			elsif (ULA_COM_2n = '1') then		
				
				if (COM_2_bit = '1')	then		-- X >= 0
				
					Select_out_compair <= "010";
				
				else									-- X < 0
				
					Select_out_compair <= "011";
				
				end if;
				
			elsif (BGTZn = '1') then			-- X > 0
			
				Select_out_compair <= "100";
				
			elsif (BLEZn = '1') then			-- X <= 0

				Select_out_compair <= "101";
				
			else										-- others

				Select_out_compair <= "110";
				
			end if;
		end if;
	end process;
	
end ALU_controller_arc;