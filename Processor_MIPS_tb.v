module Processor_MIPS_tb ();

	reg clk, rst;
	wire [31:0] bridge_A;
	Processor_MIPS Processor_MIPS
	(
		.clk(clk),
		.rst(rst),

		.bridge_A(bridge_A)
	);
	
	initial 
	begin
		clk = 0;
		rst = 1;
		#100;
		rst = 0;
	end
	
	always #10 clk <= ~clk;
	
endmodule 