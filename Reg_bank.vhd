library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Reg_bank is
	port(
		clk : in std_logic;
		rst : in std_logic;
		
		addr_rd1: in std_logic_vector(4 downto 0);
		addr_rd2: in std_logic_vector(4 downto 0);
		
		addr_wr : in std_logic_vector(4 downto 0);
		data_wr 		: in std_logic_vector(31 downto 0);
		wr_en			: in std_logic;
		
		data_wr_Link 	: in std_logic_vector(31 downto 0);
		wr_en_Link		: in std_logic;
		
		data_rd1 : out std_logic_vector(31 downto 0);
		data_rd2 : out std_logic_vector(31 downto 0)
	);
end entity;

architecture Reg_bank_arc of Reg_bank is

	type Bank is array(0 to 31) of std_logic_vector(31 downto 0);
	signal Bank_reg : Bank;
	
	signal data_rd1_int,data_rd2_int : std_logic_vector(31 downto 0);

begin
	
	process(rst,clk)
	begin
		if (rising_edge(clk)) then
			if (rst = '1') then
				data_rd1_int <= (others => '0');
				data_rd2_int <= (others => '0');
				for I in 0 to 31 loop
					Bank_reg(I) <= (others => '0');
				end loop;
			else
				data_rd1_int <= Bank_reg(to_integer(unsigned(addr_rd1)));
				data_rd2_int <= Bank_reg(to_integer(unsigned(addr_rd2)));
				if wr_en = '1' then
					Bank_reg(to_integer(unsigned(addr_wr))) <= data_wr;
				end if;
				if wr_en_Link = '1' then			
					Bank_reg(31) <= data_wr_Link;
				end if;
			end if;
		end if;
		
	end process;
	
	data_rd1 <= data_rd1_int;
	data_rd2 <= data_rd2_int;
end Reg_bank_arc;