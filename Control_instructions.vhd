library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Control_instructions is
	port (
		Instruct : in std_logic_vector(5 downto 0);
		Link_poss : in std_logic;
		
		wr_en_RAM : out std_logic;
		
		wr_en_Bk		: out std_logic;
		Dst_wr_Bk	: out std_logic;
		Type_imediate: out std_logic_vector(3 downto 0);
		Data_wr_chsr: out std_logic;
		
		IsLink		: out std_logic;
		
		JUMPn			: out std_logic;
		BEQn 			: out std_logic;
		BNEn    		: out std_logic;
		BLEZn   		: out std_logic;
		BGTZn			: out std_logic;
		ULA_COM_2n	: buffer std_logic;
		IsCOM1		: out std_logic
		
	);
end entity;

architecture Control_instructions_arc of Control_instructions is

	constant ULA_COM_1	: std_logic_vector(5 downto 0) := "000000";
--	constant ADD  			: std_logic_vector(5 downto 0) := "000000";
--	constant ADDU   		: std_logic_vector(5 downto 0) := "000000";
--	constant ANDn 			: std_logic_vector(5 downto 0) := "000000";
--	constant DIV  			: std_logic_vector(5 downto 0) := "000000";
--	constant DIVU  		: std_logic_vector(5 downto 0) := "000000";
--	constant MULT   		: std_logic_vector(5 downto 0) := "000000";
--	constant MULTU    	: std_logic_vector(5 downto 0) := "000000";
--	constant ORn    		: std_logic_vector(5 downto 0) := "000000";
--	constant SLLn   		: std_logic_vector(5 downto 0) := "000000";
--	constant SLLV  		: std_logic_vector(5 downto 0) := "000000";
--	constant SRAn   		: std_logic_vector(5 downto 0) := "000000";
--	constant SRLn     	: std_logic_vector(5 downto 0) := "000000";
--	constant SRLV    		: std_logic_vector(5 downto 0) := "000000";
--	constant SUB     		: std_logic_vector(5 downto 0) := "000000";
--	constant SUBU      	: std_logic_vector(5 downto 0) := "000000";
--	constant XORn   		: std_logic_vector(5 downto 0) := "000000";

--	constant SLT    		: std_logic_vector(5 downto 0) := "000000";
--	constant SLTU   		: std_logic_vector(5 downto 0) := "000000";

--	constant SYSCALL    	: std_logic_vector(5 downto 0) := "000000";
--	constant JR   			: std_logic_vector(5 downto 0) := "000000";
--	constant MFHI   		: std_logic_vector(5 downto 0) := "000000";
--	constant NOOP   		: std_logic_vector(5 downto 0) := "000000";
--	constant MFLO   		: std_logic_vector(5 downto 0) := "000000";
	
	-------- branches with greater and less to zero
	constant ULA_COM_2  	: std_logic_vector(5 downto 0) := "000001";
--	constant BGEZ  		: std_logic_vector(5 downto 0) := "000001";
--	constant BGEZAL  		: std_logic_vector(5 downto 0) := "000001";
--	constant BLTZ  		: std_logic_vector(5 downto 0) := "000001";
--	constant BLTZAL    	: std_logic_vector(5 downto 0) := "000001";
	
	--------- branches ----------
	constant JUMP 			: std_logic_vector(5 downto 0) := "000010";
	constant JAL   		: std_logic_vector(5 downto 0) := "000011";
	constant BEQ 			: std_logic_vector(5 downto 0) := "000100";
	constant BNE    		: std_logic_vector(5 downto 0) := "000101";
	constant BLEZ   		: std_logic_vector(5 downto 0) := "000110";
	constant BGTZ  		: std_logic_vector(5 downto 0) := "000111";	
	-----------------------------
	
	------- ULA commands for data immediate ------
	constant ADDI 			: std_logic_vector(5 downto 0) := "001000";
	constant ADDIU   		: std_logic_vector(5 downto 0) := "001001";
	constant SLTI    		: std_logic_vector(5 downto 0) := "001010";
	constant SLTIU    	: std_logic_vector(5 downto 0) := "001011";	
	constant ANDI 			: std_logic_vector(5 downto 0) := "001100";
	constant ORI    	 	: std_logic_vector(5 downto 0) := "001101";
	constant XORI     	: std_logic_vector(5 downto 0) := "001110";
	constant LUI  			: std_logic_vector(5 downto 0) := "001111";
	-----------------------------------------------
	
	-------- Operates from memory	---------
	constant LB   			: std_logic_vector(5 downto 0) := "100000";
	constant SB   			: std_logic_vector(5 downto 0) := "101000";
	constant LW  			: std_logic_vector(5 downto 0) := "100011";
	constant SW    		: std_logic_vector(5 downto 0) := "101011";
	---------------------------------------
	
	signal IsImediate, IsCOM_1, IsStore, IsLoad, JALn: std_logic;
begin
	--------- Identifica o tipo de instrucao --------
	IsCOM1 		<=	IsCOM_1;
	IsCOM_1 		<= '1' when Instruct = ULA_COM_1 else '0';
	IsImediate 	<= '1' when Instruct(5 downto 3) = "001" else '0';
	IsStore		<= '1' when Instruct(5 downto 3) = "101" else '0';
	IsLoad		<= '1' when Instruct(5 downto 3) = "100" else '0';
	-------
	
	
	--------- Configura escrita no reg_bank ------
	wr_en_Bk		 <= IsCOM_1 or IsImediate or IsLoad;
	--------- Analisa se e imediato o operador da ALU
	Dst_wr_Bk	 <= IsImediate;
	Type_imediate <= Instruct(3 downto 0);
	--------- Analisa se o dado é carregado da memoria
	Data_wr_chsr <= IsLoad;
	----------------------------------------------
	
	--------- Configura escrita na mem de dados ------
	wr_en_RAM	 <= IsStore;
	----------------------
	
	--------- Configura saidas para identificar branches ------
	JUMPn 		<= '1' when Instruct = JUMP 	else '0';
	JALn   		<= '1' when Instruct = JAL 	else '0';
	BEQn 			<= '1' when Instruct = BEQ 	else '0';
	BNEn    		<= '1' when Instruct = BNE 	else '0';
	BLEZn   		<= '1' when Instruct = BLEZ 	else '0';
	BGTZn			<= '1' when Instruct = BGTZ 	else '0';
	ULA_COM_2n	<= '1' when Instruct = ULA_COM_2 else '0';
	---------------------------------
	
	--------- analisa se a operacao de branch é com link no reg 31
	IsLink <= (ULA_COM_2n and Link_poss) or JALn;
end Control_instructions_arc;