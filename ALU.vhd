library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

------ ALU assincrona -----
entity ALU is
	port(
		data_a : in std_logic_vector(31 downto 0);
		data_b : in std_logic_vector(31 downto 0);
		
		op_addr 		: in std_logic_vector(3 downto 0);
		op_compair 	: in std_logic_vector(2 downto 0);
		data_b_type : in std_logic;
		enable_op	: in std_logic;
		
		branch_enable	: out std_logic;
		data_out : out std_logic_vector(31 downto 0)
		);
end entity;


architecture ALU_arch of ALU is
	signal sum, sum_sign: std_logic;
	signal overflow_sum, overflow_sum_sign: std_logic;
	signal data_o_ADD_SUB, data_o_ADD_SUB_SIGN : std_logic_vector(31 downto 0);

	signal data_o_AND, data_o_OR, data_o_XOR, data_o_SHFT : std_logic_vector(31 downto 0);
	signal data_o_MULT, data_o_MULT_SIGN : std_logic_vector(63 downto 0);
	signal data_o_DIV, resto_DIV, data_o_DIV_SIGN, resto_DIV_SIGN: std_logic_vector(31 downto 0);

	signal right_left, data_complete : std_logic;
	constant ADD 				: std_logic_vector(3 downto 0) := "1000";
	constant ADDU			 	: std_logic_vector(3 downto 0) := "1001";
	constant ANDn  			: std_logic_vector(3 downto 0) := "1100";
	constant DIVn			  	: std_logic_vector(3 downto 0) := "0011";
	constant DIVu 				: std_logic_vector(3 downto 0) := "0100";
	constant MULTn				: std_logic_vector(3 downto 0) := "0000";
	constant MULTu 			: std_logic_vector(3 downto 0) := "0001";
	constant ORn 				: std_logic_vector(3 downto 0) := "1101";
	constant SLLn				: std_logic_vector(3 downto 0) := "0000";
	constant SLT 				: std_logic_vector(3 downto 0) := "1010";
	constant SLTu 				: std_logic_vector(3 downto 0) := "1011";
	constant SRAn				: std_logic_vector(3 downto 0) := "0010";
	constant SRLn				: std_logic_vector(3 downto 0) := "0011";
	constant SUB				: std_logic_vector(3 downto 0) := "1010";
	constant SUBu				: std_logic_vector(3 downto 0) := "1011";
	constant XORn				: std_logic_vector(3 downto 0) := "1110";
	
	
	component ADD_SUB 
		PORT
		(
			add_sub		: IN STD_LOGIC ;
			dataa		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			datab		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			overflow		: OUT STD_LOGIC ;
			result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
		);
	end component;
	
	component MULT
		PORT
		(
			dataa		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			datab		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
		);
	end component;
	
	component DIV
		PORT
		(
			denom		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			numer		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			quotient		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
			remain		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
		);
	end component;
	
	component Compare
		PORT
		(
			dataa		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			datab		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			aeb		: OUT STD_LOGIC ;
			agb		: OUT STD_LOGIC ;
			alb		: OUT STD_LOGIC 
		);
	end component;
	
	component ADD_SUB_SIGNED
		PORT
		(
			add_sub		: IN STD_LOGIC ;
			dataa		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			datab		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			overflow		: OUT STD_LOGIC ;
			result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
		);
	end component;
	
	component MULT_SIGN IS
		PORT
		(
			dataa		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			datab		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
		);
	end component;
	
	component DIV_SIGN
		PORT
		(
			denom		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			numer		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			quotient		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
			remain		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
		);
	end component;
			
	component shift_block is
	port(
		data: in std_logic_vector(31 downto 0);
		
		shift_value: in std_logic_vector(4 downto 0);
		right_left: in std_logic;
		
		data_complete : in std_logic;
		
		data_out : out std_logic_vector(31 downto 0)
		
	);
	end component;

	
begin
	--------------- Add and Sub operations -----------------
	--------------------------------------------------------
	sum <= '1' when (op_addr = ADD) or (op_addr = ADDU) else '0';
--	sum_sign <= '1' when (op_addr = ADD) else '0';
	ADD_SUB_Inst: ADD_SUB
		port map
		(
			add_sub	=> sum, 
			dataa		=> data_a,
			datab		=> data_b,
			overflow	=> overflow_sum,
			result	=> data_o_ADD_SUB
		);
	
	ADD_SUB_signed_Inst: ADD_SUB_SIGNED
		port map
		(
			add_sub	=> sum, 
			dataa		=> data_a,
			datab		=> data_b,
			overflow	=> overflow_sum_sign,
			result	=> data_o_ADD_SUB_SIGN
		);
	--------------------------------------------------------
	
--	--------------- And and Or  operations -----------------
--	--------------------------------------------------------
--	data_o_AND 	<= data_a and data_b;
--	data_o_OR 	<= data_a or data_b;
--	data_o_XOR 	<= data_a xor data_b;
--	--------------------------------------------------------
--	
--	---------------- Shift Right and Left ------------------
--	shift_block_inst: shift_block
--		port map(
--			data	=> data_a,
--			
--			shift_value	=> data_b(4 downto 0),
--			right_left  => right_left,
--			
--			data_complete => data_complete,
--			
--			data_out => data_o_SHFT
--		);
--	--------------------------------------------------------
--	
--	------------- Multiplication operations ----------------
--	--------------------------------------------------------	
--	MULT_inst: MULT
--		port map
--		(
--			dataa	=> data_a,
--			datab	=> data_b,
--			result => data_o_MULT
--		);
--		
--	MULT_SIGN_inst: MULT_SIGN
--		port map
--		(
--			dataa	=> data_a,
--			datab	=> data_b,
--			result => data_o_MULT_SIGN
--		);
--	--------------------------------------------------------
--	
--	---------------- Division Operations -------------------
--	--------------------------------------------------------
--	DIV_inst: DIV
--		port map
--		(
--			denom		=> data_b,
--			numer		=> data_a,
--			quotient	=> data_o_DIV,
--			remain	=> resto_DIV
--		);
--		
--	DIV_SIGN_inst: DIV_SIGN
--		port map
--		(
--			denom		=> data_b,
--			numer		=> data_a,
--			quotient	=> data_o_DIV_SIGN,
--			remain	=> resto_DIV_SIGN
--		);
--	--------------------------------------------------------
--	
--	---------------- Operacoes de maior, menor e igual
--	Compare_inst: Compare
--		port map
--		(
--			dataa		=> data_a,
--			datab		=> data_b,
--			aeb		=> IsEqual,
--			agb		=> IsGreater,
--			alb		=>	IsLess
--		);
--	--------------------------------------------------
	
	with op_addr select data_out <=
		data_o_ADD_SUB  					when ADD,
		data_o_ADD_SUB_SIGN				when ADDU,
--		data_o_MULT(31 downto 0) 		when ANDn,
--		data_o_MULT_SIGN(31 downto 0) when DIV,
--		data_o_DIV  						when DIVu,
--		data_o_DIV_SIGN  					when MULT,
--		data_o_ADD_SUB 					when MULTu,
--		data_o_ADD_SUB 					when SLLn,
--		data_o_AND  						when SLT,
--		data_o_OR   						when SLTu,
--		data_o_MULT(31 downto 0) 		when SRAn,
--		data_o_MULT_SIGN(31 downto 0) when SRLn,
--		data_o_DIV  						when SUB,
--		data_o_DIV_SIGN  					when SUBu,
--		data_o_ADD_SUB 					when XORn,
		(others => '0') 				when others;
	
end ALU_arch;
